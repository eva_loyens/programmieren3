package networkIO;
import java.io.*;
import java.net.*;

public class HttpMitSocket
{
	public static void main(String[] args) throws UnknownHostException, IOException
	{
		// start connection to server (wetter.orf.at)
		Socket socket = new Socket("wetter.orf.at", 80);

		// my request
		String request = "GET /steiermark/prognose HTTP/1.1\r\n";
		String request2 = "Host: orf.at\r\n\r\n";

		// starts the connection to send the request to the server
		OutputStream ostream = socket.getOutputStream();

		// to send request to the server
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(ostream));

		// send request
		writer.write(request);
		writer.write(request2);
		writer.flush();

		// read response from server
		InputStream iStream = socket.getInputStream();
		BufferedReader reader = new BufferedReader(new InputStreamReader(iStream));

		// print the response from the server
		String line;
		while ((line = reader.readLine()) != null)
		{
			System.out.println(line);
		}

		socket.close();
	}
}
