package at.campus02.nowa.ss2018.pr3.klausur;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;


public class Server {
	public static void main(String[] args) {
		
		try (
				ServerSocket server = new ServerSocket(1111); ) {
				while (true) {
					Socket client = server.accept();
					
					// System.out.println("Accepted" + server);
					
					new Thread (new ServerRunnable(client)).start(); 
				}
				
			} catch (IOException e) {
				e.printStackTrace();
		}
		}

}
