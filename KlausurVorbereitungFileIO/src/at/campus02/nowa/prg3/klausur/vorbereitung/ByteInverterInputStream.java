package at.campus02.nowa.prg3.klausur.vorbereitung;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

public class ByteInverterInputStream extends FilterInputStream {

	protected ByteInverterInputStream(InputStream arg0) {
		super(arg0);
	}
	
	@Override
	public int read() throws IOException {
		
		int b = super.read();
			if(b == -1) {
			return b;
			}
		return Integer.reverse(b << 24 &0xff);
	}
	
}
