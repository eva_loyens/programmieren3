package FileIO;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.regex.Pattern;

public class FileWriterNotenStudentenEinlesen
{

	public static void main(String[] args)
	{
		//write and read from/to file
		//also creates file if it does not exists
		try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
				PrintWriter pw = new PrintWriter(new OutputStreamWriter(new FileOutputStream("benotung.csv"), StandardCharsets.UTF_8)))
		{

			Pattern pname = Pattern.compile("[^;]+");
			Pattern pnote = Pattern.compile("[1-5]");
			while (true)
			{
				System.out.println("Name: ");
				String name = br.readLine();
				if (name == null || name.equals("STOP") || name.equals("stop"))
				{
					System.out.println("Stream wird geschlossen!");
					break;
				}
				if (!pname.matcher(name).matches())
				{
					System.out.println("Falsche Eingabe!");
					continue;
				}
				System.out.println("Note:");
				String note = br.readLine();
				if (note == null || note.equals("STOP") || note.equals("stop"))
				{
					System.out.println("Stream wird geschlossen!");
					break;
				}

				if (!pnote.matcher(note).matches())
				{
					System.out.println("Falsche Eingabe!");
					continue;
				}
				pw.println(name + ";" + note);
				pw.flush();
			}
		} catch (FileNotFoundException e)
		{
			e.printStackTrace();
		} catch (IOException e1)
		{
			e1.printStackTrace();
		}

	}

}
