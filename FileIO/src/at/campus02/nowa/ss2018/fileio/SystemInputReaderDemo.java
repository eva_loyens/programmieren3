package at.campus02.nowa.ss2018.fileio;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
//import java.io.Reader;

public class SystemInputReaderDemo {
	
	public static void main (String[] args) {
//		Reader in = new InputStreamReader(System.in);
//		BufferedReader br = new BufferedReader(in);
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		String line;
		try {
			while ((line = br.readLine()) != null) {
				switch (line) {
				case "HELP":
					System.out.println("HELP? WOMIT?");				
				break;
				case "STOP":
					System.out.println("STOP: Stoppt die Anwendung");
				return;
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}

