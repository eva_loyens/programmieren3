package at.campus02.nowa.prg3.klausur.fileio;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

public class ASCIIInputStream  extends FilterInputStream{

	protected ASCIIInputStream(InputStream in) {
		super(in);
	}

	@Override // source schauen welche methoden uberschrieben werden k�nnen >> mit aufgabe vergleichen!!
	// in diesem fall >> read()
	public int read() throws IOException {
		
		int b;
		while((b = super.read()) != -1) // wir lesen bis ende der datei = -1
			// lese solangfe bis dus das ende erreciht hast, also solange bis das lesen nicht -1 ist
		{
			if (b < 128) {
			return b;
			} 		
	}
		return b;
	}

}
