package at.campus02.nowa.prg3.klausur.vorbereitung;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class Client {

	public static void main(String[] args) {
		try (
			Socket client = new Socket(InetAddress.getLocalHost(), 4000);
			BufferedReader br = new BufferedReader(new InputStreamReader(client.getInputStream()));
			PrintWriter pw = new PrintWriter(client.getOutputStream());
			BufferedReader brc = new BufferedReader(new InputStreamReader(System.in));
		) {
			String line;
			while ((line = brc.readLine()) != null) {
				pw.println(line);
				pw.flush();
				String answer = br.readLine();
				System.out.println(answer);
			}
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
