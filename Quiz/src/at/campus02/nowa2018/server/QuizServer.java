package at.campus02.nowa2018.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class QuizServer {

	public static void main(String[] args) {

		ServerSocket server;
		GameManager gm = new GameManager();
		
		try {
			server = new ServerSocket(1111);
			while (true) {
				Socket socket = server.accept();
				System.out.println("Accepted: " + socket);
				ClientRunnable cr = new ClientRunnable(socket, gm);
				Thread t = new Thread(cr);
				t.start();
			}
	}	catch (IOException e) {
			e.printStackTrace();
		}
	}
}
