package at.campus02.nowa2018.server;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;


public class GameManager {
	
	//zufallszahlengenerator ertsellen
	private int playerNum; // max 4 spieler pro spiel, hier �ndern wenn mann m�chte
	private Random random = new Random();
	private List<QuestionRecord> questions = new ArrayList<>();
	private List<ClientRunnable> clients = new ArrayList<>();
	private Map <ClientRunnable, Boolean> answers = new HashMap<>();
	private Map <String, Integer > scores = new HashMap<>();
	public QuestionRecord currentQuestion;
	
	

	public GameManager() {
		try (FileInputStream fin = new FileInputStream("server.properties")) {
			Properties config = new Properties();
			config.load(fin);
			playerNum = Integer.parseInt(config.getProperty("players.number"));
		} catch (NumberFormatException e1) {
			playerNum = 2;
		} catch (FileNotFoundException e1) {
			playerNum = 2;
		} catch (IOException e1) {
			System.out.println("Konfiguration konnte nicht gelesen werden: " + e1.getMessage());
			//alles was "-" ist: fehler, alles was " + " ist, succesvoll
			System.exit(-1);
		}
		try(
				BufferedReader br = new BufferedReader(new InputStreamReader
						(new FileInputStream("quizfragen.csv"), StandardCharsets.UTF_8));
		) {
			String line;
			while((line = br.readLine()) != null) {
				if (line.contains("?")) {
					System.out.println(line);
				}
				
				String[] fields = line.replace("\"", "").split(";");
				if (fields.length !=7) {
					continue;
				}
				
				try {// das letzte feld = 7. feld in der quiz-datei ist einen int
				Integer.parseInt(fields[6]);
				} catch (NumberFormatException e) {
					System.out.println("Falsche Zahl: " + fields[6]);
					continue;
				}
								
				QuestionRecord qr = new QuestionRecord(fields[0], Integer.parseInt(fields[6]), new AnswerRecord(fields[1]));
				qr.getWrong().add(new AnswerRecord(fields[2]));
				qr.getWrong().add(new AnswerRecord(fields[3]));
				qr.getWrong().add(new AnswerRecord(fields[4]));
				questions.add(qr);
				
				System.out.println(line.replace("\"", ""));
			}
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public synchronized QuestionRecord getRandomQuestion() {
		// mit synchronized vermeiden wir dass mehrere threads gleichzeitig lesen und schreiben
		// (remove = art von schreiben-operation) koennen
		
				int r = random.nextInt(questions.size());
				// frage koennte doppelt kommen
				QuestionRecord q = questions.get(r);
				questions.remove(r);
				return q;
				
	}
	
	public Map<String, Integer> getScores() {
		return scores;
	}

	public void setScores(Map<String, Integer> scores) {
		this.scores = scores;
	}

	public synchronized boolean registerClient(ClientRunnable r) {
		clients.add(r);
		if (clients.size() == playerNum) {
			currentQuestion = getRandomQuestion();
			for (ClientRunnable cr : clients) {
				synchronized (cr) {
					cr.notify();
				}
			}
			return true;
		}
		return false;
	}

	public QuestionRecord getCurrentQuestion() {
		return currentQuestion;
	}

	public boolean setAnswer(ClientRunnable clientRunnable, boolean correct) {
		answers.put(clientRunnable, correct);
		if (!scores.containsKey(clientRunnable.getPlayerName())) {
			scores.put(clientRunnable.getPlayerName(), 0);
		}
		
			if (correct) {
				int currentScore = scores.get(clientRunnable.getPlayerName());
				int newScore = currentScore + currentQuestion.getLevel();
				scores.put(clientRunnable.getPlayerName(), newScore);
			}
			if (answers.size() == playerNum) {
			currentQuestion = getRandomQuestion();
			for (ClientRunnable cr : clients) {
				synchronized (cr) {
					cr.notify();
				}
			}
			
			answers.clear();
			return true;
		}
		return false;
	}
}
