package at.campus02.nowa.prg3.klausur.vorbereitung;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class ServerRunnable implements Runnable {
	
	private Socket client;
	private static int summe = 0;
	private static int counter = 0;
	
	public ServerRunnable(Socket client) {
		super();
		this.client = client;
	}

	@Override
	public void run() {
		try (PrintWriter pw = new PrintWriter(client.getOutputStream());
				BufferedReader br = new BufferedReader(new InputStreamReader(client.getInputStream()))) {
			String line = null;
			while ((line = br.readLine()) != null) {
				try {
					synchronized (ServerRunnable.class) {
						int in = Integer.parseInt(line);
						ServerRunnable.summe += in;
						ServerRunnable.counter++;
					}
					pw.println("Aktueller Durchschnitt aller empfangener Integer: " + (float)ServerRunnable.summe / ServerRunnable.counter);
				} catch (NumberFormatException e) {
					pw.println("Keine gueltige Zahl uebertragen");
				}
				pw.flush();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			client.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
		
