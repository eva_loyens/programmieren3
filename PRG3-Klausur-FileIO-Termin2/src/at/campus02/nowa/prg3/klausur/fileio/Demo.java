package at.campus02.nowa.prg3.klausur.fileio;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class Demo {

	public static void main(String[] args) {
		
		try (
			ASCIIInputStream ais = new ASCIIInputStream(new FileInputStream("input.txt")); ) {
			
			int b;
			while ((b = ais.read()) != -1) {
				System.out.println(b);
			}
			ais.close();
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}

}
