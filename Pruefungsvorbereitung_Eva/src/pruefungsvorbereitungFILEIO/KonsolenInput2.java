package pruefungsvorbereitungFILEIO;

import java.io.IOException;

public class KonsolenInput2 {

	public static void main(String[] args) {
		
		int b;
		try { 
			
			while ((b = System.in.read()) !=-1) {
				char c = Character.toChars(b)[0];
				if (c == 'x' || c== 'X') {
					System.out.println("Einlesen wird gestopt");
					return;
				}
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
