package at.campus02.nowa2018;

public class MultithreadSleep {

	@SuppressWarnings("deprecation")
	public static void main(String[] args) {
		Thread t1 = new TimerThread();
		Thread t2 = new TimerThread();
		Thread t3 = new TimerThread();
		Thread t4 = new TimerThread();
		
		t1.start();
		t2.start();
		t3.start();
		t4.start();
		
		// stop soll mann nicht benutzen = holzhammer methode!!
		// deswegen jammert er auch >> ist auch deprecated
		// jammern habe ich mit suppresswarnings (sehe oben) unterdruckt 
		
		try {
			Thread.sleep(1000);
			t1.stop();
			Thread.sleep(1000);
			t2.stop();
			Thread.sleep(1000);
			t3.stop();
			Thread.sleep(1000);
			t4.stop();
			Thread.sleep(1000);
			System.out.println("Fertig");
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
