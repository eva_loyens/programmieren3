package at.campus02.nowa.prg3.klausur.vorbereitung;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.junit.Test;

public class ByteInverterInputStreamTest {
	
	private InputStream secret = new ByteArrayInputStream(DatatypeConverter.parseHexBinary("6A96A63604A24E66F636E60446A6960426A64E04D23686AECEAE4E8450"));
	private byte[] plain = DatatypeConverter.parseHexBinary("5669656C204572666F6C672062656920646572204B6C6175737572210A");

	@Test
	public void testFullRead() throws IOException {
		secret.reset();
		ByteInverterInputStream decoder = new ByteInverterInputStream(secret);
		int buf;
		int index = 0;
		while ((buf = decoder.read()) != -1) {
			assertEquals(plain[index], (byte)buf);
			index++;
		}
		decoder.close();
	}

}
