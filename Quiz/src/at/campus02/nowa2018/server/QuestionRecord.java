package at.campus02.nowa2018.server;

import java.util.ArrayList;
import java.util.List;

public class QuestionRecord {
	
	private String text;
	private int level;
	private AnswerRecord correct;
	private List<AnswerRecord> wrong = new ArrayList<>();
	
	public QuestionRecord(String text, int level, AnswerRecord correct) {
		super();
		this.text = text;
		this.level = level;
		this.correct = correct;
	}

	public String getText() {
		return text;
	}

	public int getLevel() {
		return level;
	}

	public AnswerRecord getCorrect() {
		return correct;
	}

	public List<AnswerRecord> getWrong() {
		return wrong;
	}	

}
