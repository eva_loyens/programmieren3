package at.campus02.nowa.ss2018.pr3.klausur;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ServerRunnable implements Runnable {


	private Socket client;
	private ArrayList<Server> allClients = new ArrayList<>();

	public ServerRunnable(Socket client) {
		super();
		this.client = client;
	}

	@Override
	public void run() {
		try (	
				BufferedReader reader = new BufferedReader(new InputStreamReader(client.getInputStream()));
				PrintWriter writer = new PrintWriter(new OutputStreamWriter(client.getOutputStream()));)
		{
			String line = null;
			Date now = new Date();
	
			for ( 
					// each what in what??
					) {
				allClients.add(client);	}

			while ((line = reader.readLine()) != null) {
					String output;
					if (line.equalsIgnoreCase("time"))
					{
						output = ("Current Time: " + now.toString());
					} else if (line.equalsIgnoreCase("clients"))
					{
						output = ("Connected clients : " + allClients.size());
					break;
					} else
					{
						output = "Unknown command";
					break;
					}
					writer.write(output);
					writer.flush();
			}
		} catch (NumberFormatException e) {
			allClients.remove(client);
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}	try {
			client.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
