package at.campus02.nowa.prg3.klausur.fileio;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class ASCIIInputStreamTest {

	private ByteArrayInputStream input = new ByteArrayInputStream(
			"T�pfelhy�nen�hrchen".getBytes(StandardCharsets.ISO_8859_1)
			);
	private final List<Integer> expected = new ArrayList<>();

	@Before
	public void setUp() throws Exception {
		expected.add(84);
		expected.add(112);
		expected.add(102);
		expected.add(101);
		expected.add(108);
		expected.add(104);
		expected.add(121);
		expected.add(110);
		expected.add(101);
		expected.add(110);
		expected.add(104);
		expected.add(114);
		expected.add(99);
		expected.add(104);
		expected.add(101);
		expected.add(110);
	}

	@Test
	public void testRead() throws IOException {
		ASCIIInputStream stats = new ASCIIInputStream(input);
		List<Integer> read = new ArrayList<>();
		int b;
		while ((b = stats.read()) != -1) {
			read.add(b);
		}
		assertEquals(expected.size(), read.size());
		assertEquals(expected, read);
		stats.close();
	}

}
