package at.campus02.nowa2018;

public class TimerThread extends Thread {

	@Override
	public void run() {
		while (true) {
			System.out.println("Thread: " + Thread.currentThread().getId());
			try {
				Thread.sleep(200);
				//threads koennen exceptions werfen, deswegen try-catch-block
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		
		}
	}

}

