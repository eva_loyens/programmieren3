package at.campus02.nowa.ss2018.fileio;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class Application {

	public static void main(String[] args) throws LustigeExceptions { 
		
		try {
			System.in.available();
			// .available = kann ich da noch ein byte lesen, liefert einen int zur�ck
			// .read liefert einen int zur�ck
			// .read[] > array gr�ssere bl�cke lesen, 1024 bytes lesen pro array
			// 		gibt einen int zur�ck > wieviel bytes hat rechner gebraucht
			System.out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try {
			File f = new File("E:\\test.txt");
			// Es gibt kein laufwerk E, das heisst es wird einen Fehler kommen 
			System.out.println(f.getAbsolutePath());
			if (!f.exists()) {
				f.createNewFile();
				throw new LustigeExceptions();
			}
		} catch (IOException e) {
			System.out.println("Wir haben einen Fehler fesstgestellt!");
			LustigeExceptions le = new LustigeExceptions();
			le.addSuppressed(e);
			throw le;
		} catch (LustigeExceptions e) {
			System.out.println("Wir haben einen lustigen Fehler festgestellt!");
		}
		
		
			File f = new File("test1.txt");
			File f2 = new File("blax/bla/bar");
			System.out.println(f.getAbsolutePath());
			System.out.println(f2.getAbsolutePath());
			
			if (!f.exists()) {
				System.out.println(f.mkdirs());
				try {
				f.createNewFile();
				}
				catch (IOException e) {
					System.out.println("Fehler beim erstellen der Datei : " + e.getMessage());
					return;
				}
			}
				
			if (f.isDirectory()) {
				for (String l : f.list(new TxtFilenameFilter())) {
					System.out.println(l);				
				}	
			}

//			System.out.println(f.toURI());
//			System.out.println(f.pathSeparator);
//			System.out.println(f.separator);
//			System.out.println(f2.separator);
			
			try {
				FileInputStream inp = new FileInputStream("test1.txt");
				// ist gleich wie new FileInputStream(f); f verweist auf test1 // sehe oben
				
				int b;
				while ((b = inp.read()) != -1) {
					System.out.println(b);
					System.out.println(Character.toChars(b)); // macht aus code(bytes) lesbare text(buchstaben)
				}
				inp.close();
				
			} catch (FileNotFoundException e1) {
				System.out.println("File not found: " + e1.getMessage());
				return;
			} catch (IOException e) {
				System.out.println("File could not be read: " + e.getMessage());
				return;
			}
			
	}
}
