package at.campus02.nowa2018.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import at.campus02.nowa2018.messages.Answer;
import at.campus02.nowa2018.messages.Player;
import at.campus02.nowa2018.messages.Question;

public class Client {

	public static void main(String[] args) {
	
		try (
				Socket socket = new Socket("localhost", 1111);
				ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
				ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
				// system.in >> eingabe von der console
				BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			) {
				System.out.println("Bitte Spielernamen eingeben:");
				String name = br.readLine();
				Player p = new Player(name);
				oos.writeObject(p);
				oos.flush();
				
				while (true) {		
				Question q = (Question) ois.readObject();
				System.out.println(q.getText() + "(Schwierigkeit: " + q.getLevel() + ")");
				
				int prefix = 1;
				Map<Integer, UUID> prefixMap = new HashMap<>();
				for (Entry<UUID, String> entry : q.getAnswers().entrySet()) {
					System.out.println(" " + prefix + ": " + entry.getValue());
					prefixMap.put(prefix,  entry.getKey());
					prefix++;	
				}
				String answer;
				int id;
				while (true) {
					answer = br.readLine();
					if (answer == null) {
						System.out.println("Auf wiedersehen, selber Schuld!");
						return;
					}
					
					try {
						id = Integer.parseInt(answer);
						if (prefixMap.containsKey(id)) {
							break;
							}
					} catch (NumberFormatException e) {
							System.out.println("Eingabe ung�ltig!");
					}
				}
				prefixMap.get(id);
				Answer a = new Answer(prefixMap.get(id));
				
				oos.writeObject(a);
				oos.flush();
				
				Answer as = (Answer) ois.readObject();
				if (as.getGuess().equals(prefixMap.get(id))) {
					System.out.println("Korrekt");
					System.out.println("----------");
				} else {
					System.out.println("Falsche Antwort! Richtig: " + q.getAnswers().get(as.getGuess()));
					System.out.println("----------");
				}
				
				if (q.isLast()) {
					break;
					}
				}
				
				@SuppressWarnings("unchecked")
				Map <String , Integer> scores = (Map<String, Integer>) ois.readObject();
				
				int maxValue = 0;
				String winner = " ";
				List<String> winners = new ArrayList<>();
				
				for (Entry<String, Integer> score : scores.entrySet()) {
						
						if (score.getValue() >= maxValue) {
							maxValue = score.getValue();
							winner = score.getKey();
					}
						
					System.out.println("Spieler " + score.getKey() + " hat " + score.getValue() + " Punkte.");
				}
				System.out.println("----------");
				
				for (Entry<String, Integer> score : scores.entrySet()) {
					if (score.getValue() == maxValue) {
						winners.add(score.getKey());
					}
				}
				
				if (winners.size() == 1) {
					
					System.out.println("Gewonnen hat " + winners.get(0) + " mit " + maxValue + " Punkten.");
				} else if (winners.size() > 1) {
					System.out.println("Gewonnen haben: ");
					for (String w : winners) {
						System.out.println(w);
					}
					
				}
			
				System.out.println("*-*-*-*-*-*");
				System.out.println("Danke f�rs Spielen!");
				
			} catch (UnknownHostException e) {
				e.printStackTrace(); 
			} catch (IOException e) {
				e.printStackTrace(); 
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
	}
}
