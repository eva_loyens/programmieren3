package at.campus02.nowa2018;

public class TimerRunnable implements Runnable {
	
	public boolean isRunning = true;
	
	public void requestStop() {
		this.isRunning = false;
	}

	public void run() {
		
		while (isRunning) {
			System.out.println("Runnable: " + Thread.currentThread().getId());
			try {
				Thread.sleep(200);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}