package FileIO;

import java.io.IOException;

public class KonsolenInput
{
	// listens to the input until it gets the sign to stop
	public static void main(String[] args)
	{
		int b;
		try
		{
			while ((b = System.in.read()) != -1)
			{
				char c = Character.toChars(b)[0];
				if (c == 'x' || c == 'X')
				{
					System.out.println("Der Stream wird geschlossen!");
					return;
				}
			}
		} catch (IOException e)
		{
			e.printStackTrace();
		}

	}

}
