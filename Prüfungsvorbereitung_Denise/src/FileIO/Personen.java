package FileIO;

import java.io.Serializable;
import java.util.Date;

public class Personen implements Serializable {
	
	private String vorname;
	private String nachname;
	private Date geburtstag;

	public String getVorname() {
		return vorname;
	}

	public String getNachname() {
		return nachname;
	}

	public Date getGeburtstag() {
		return geburtstag;
	}

	public Personen(String vorname, String nachname, Date geburtstag) {
		super();
		this.vorname = vorname;
		this.nachname = nachname;
		this.geburtstag = geburtstag;
	}

}
