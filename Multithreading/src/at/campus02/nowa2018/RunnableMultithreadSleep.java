package at.campus02.nowa2018;

public class RunnableMultithreadSleep {

	public static void main(String[] args) {
		
		TimerRunnable r1 = new TimerRunnable();
		TimerRunnable r2 = new TimerRunnable();
		TimerRunnable r3 = new TimerRunnable();
		TimerRunnable r4 = new TimerRunnable();
		TimerRunnable r5 = new TimerRunnable();
		
		Thread t1 = new Thread(r1);
		Thread t2 = new Thread(r2);
		Thread t3 = new Thread(r3);
		Thread t4 = new Thread(r4);
		Thread t5 = new Thread(r5);
		
		
		t1.start();
		t2.start();
		t3.start();
		t4.start();
		t5.start();
		
		try {
			Thread.sleep(10000);
			
			r1.requestStop();
			r2.requestStop();
			r3.requestStop();
			r4.requestStop();
			r5.requestStop();
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
