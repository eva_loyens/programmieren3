	package at.campus02.nowa.prg3.klausur.networkio;
	
	import java.io.IOException;
	import java.net.ServerSocket;
	import java.net.Socket;
	
	public class Server {
	
	public static void main(String[] args) {
	
		ServerSocket server;
		try {
			server = new ServerSocket(4000);
			while (true) {
				Socket socket = server.accept();
				// System.out.println("Accepted: " + socket);
				ClientRunnable cr = new ClientRunnable(socket);
				Thread t = new Thread(cr);
				t.start();
			}
		}	catch (IOException e) {
			System.out.println("Could not listen to the client " + e.getMessage());
			e.printStackTrace();
			}
		}
}
