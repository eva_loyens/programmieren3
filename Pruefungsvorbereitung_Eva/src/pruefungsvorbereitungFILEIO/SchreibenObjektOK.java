package pruefungsvorbereitungFILEIO;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;


public class SchreibenObjektOK {

	public static void main(String[] args) {
		
		String notiz = "Hallo, dies ist ein text";
			
			try (	OutputStream os = new FileOutputStream("datei.txt");
					ObjectOutputStream oos = new ObjectOutputStream(os);
					InputStream is = new FileInputStream("datei.txt");
					ObjectInputStream ois = new ObjectInputStream(is)) {
				
						oos.writeObject(notiz);
						oos.flush();
				
						String geleseneNotiz = (String) ois.readObject();
						System.out.println(geleseneNotiz);
					}
			catch (IOException e) {
				e.printStackTrace(); } 
			catch (ClassNotFoundException e) {
				e.printStackTrace();
			}	
		}
	}

