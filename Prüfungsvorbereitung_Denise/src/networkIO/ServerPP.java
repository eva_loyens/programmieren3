package networkIO;
import java.io.*;
import java.net.*;

public class ServerPP
{

	public static void main(String[] args) throws IOException
	{
		ServerSocket server = new ServerSocket(1111);

		while (true)
		{
			Socket socket = server.accept();

			// read what you get
			try (BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
					BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));)
			{

				String line;
				while ((line = reader.readLine()) != null)
				{
					String output;
					if (line.equalsIgnoreCase("ping"))
					{
						output = "pong";
					} else if (line.equalsIgnoreCase("pong"))
					{
						output = "ping";
						break;
					} else
					{
						output = "Invalid input. Try again.";
						break;
					}
					writer.write(output);
					writer.flush();
				}
			} catch (IOException e)
			{
				e.printStackTrace();
			}
		}

	}

}
