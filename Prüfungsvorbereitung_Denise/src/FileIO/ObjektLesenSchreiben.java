package FileIO;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Date;

public class ObjektLesenSchreiben
{

	public static void main(String[] args)
	{
		// write and read to/from an object
		String fileName = "object.txt";
		try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(fileName)); // writes to the file
				ObjectInputStream ois = new ObjectInputStream(new FileInputStream(fileName))) // reads from the file
		{
			Personen p = new Personen("Michael", "Fladischer", new Date());
			oos.writeObject(p);
			oos.flush();

			Personen pi = (Personen) ois.readObject();
			System.out.println(pi.getNachname());
		}

		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}

		catch (IOException e)
		{
			e.printStackTrace();
		}

		catch (ClassNotFoundException e)
		{
			e.printStackTrace();
		}

	}

}
