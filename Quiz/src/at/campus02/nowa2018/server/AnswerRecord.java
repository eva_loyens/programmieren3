package at.campus02.nowa2018.server;

import java.util.UUID;

public class AnswerRecord {
	
	// UUID = zufaellig genreierte werte die man fuer unique IDs verwenden kann 
	private UUID uuid = UUID.randomUUID();
	private String text;
	

	public AnswerRecord(String text) {
		super();
		this.text = text;
	}
	
	public UUID getUuid() {
		return uuid;
	}

	public String getText() {
		return text;
	}

	
}
