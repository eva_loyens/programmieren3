package at.campus02.nowa2018.messages;

import java.io.Serializable;
import java.util.UUID;

public class Answer implements Serializable {
	
	private static final long serialVersionUID = 3L;
	private UUID guess;
	

	public Answer(UUID guess) {
		super();
		this.guess = guess;
	}


	public UUID getGuess() {
		return guess;
	}	

}
