package at.campus02.nowa2018.server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Map;

import at.campus02.nowa2018.messages.Answer;
import at.campus02.nowa2018.messages.Player;
import at.campus02.nowa2018.messages.Question;

public class ClientRunnable implements Runnable {
	
	private Socket client;
	private GameManager gm;
	private String playerName;

	public ClientRunnable(Socket client, GameManager gm) {
		super();
		this.client = client;
		this.gm = gm;
	}
	
	public void run() {
		try (
				ObjectInputStream ois = new ObjectInputStream(client.getInputStream());
				ObjectOutputStream oos = new ObjectOutputStream(client.getOutputStream());
				) {
			// es kommt ein playerobject aus dem inputstream
					Player p = (Player)ois.readObject();
					playerName = p.getName();
					
					System.out.println(p.getName());
					
					int maxRounds = 3;
					int count = 1;
					
					if (!gm.registerClient(this)) {
						synchronized (this) {
							wait();
						}
					}
				
					while(count<= maxRounds) {
						
					QuestionRecord qr = gm.getCurrentQuestion();
					Question  q = new Question(qr.getLevel(), qr.getText());
					q.getAnswers().put(qr.getCorrect().getUuid(), qr.getCorrect().getText());
					for (AnswerRecord ar : qr.getWrong()) {
						q.getAnswers().put(ar.getUuid(), ar.getText());
					}
					
					if (count == maxRounds) {
						q.setLast();
					}
					oos.writeObject(q);
					oos.flush();
					
					Answer a = (Answer)ois.readObject();
					boolean correct = a.getGuess().equals(qr.getCorrect().getUuid());
					
					if (!gm.setAnswer(this, correct)) {
						synchronized (this) {
							wait();	
						}
					}
				
					//as >> answer server
					Answer as = new Answer(qr.getCorrect().getUuid());
					oos.writeObject(as);
					oos.flush();
					
					count++;
				}
					
					Map<String, Integer> scores = gm.getScores();
					oos.writeObject(scores);
					oos.flush();
					
		
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public String getPlayerName() {
		return playerName;
	}

}