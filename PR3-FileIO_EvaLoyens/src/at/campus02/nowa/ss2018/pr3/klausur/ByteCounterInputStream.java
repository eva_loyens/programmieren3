package at.campus02.nowa.ss2018.pr3.klausur;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

public class ByteCounterInputStream extends FilterInputStream {
		
	private char value;
	private int counter;

	public ByteCounterInputStream(InputStream in, char c) {
		super(in);
		this.value = c;
	}
	
	public int read() throws IOException {
		counter = 0;
		int b = 0;
		
		if (b == value) {
			return b;
			
		}
		counter++;
		return b;
	}

	public int getCounter() {
		return counter;
	}

	public void setCounter(int counter) {
		this.counter = counter;
	}
	
	public int getCount() {
	return counter;
	}
}
