package pruefungsvorbereitungFILEIO;

import java.io.IOException;

public class KonsolenInput {
	
	public static void main (String[] args) {
		
		int b;
		try { 
			while ((b = System.in.read()) !=-1);
		
			char c = Character.toChars(b)[0];
			if (c == 'x' || c=='X') {
				System.out.println("Der Stream wird geschlossen!");
				return;
			}	
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}

}
