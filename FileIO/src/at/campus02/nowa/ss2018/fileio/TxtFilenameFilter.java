package at.campus02.nowa.ss2018.fileio;

import java.io.File;
import java.io.FilenameFilter;

public class TxtFilenameFilter implements FilenameFilter {

	@Override
	public boolean accept(File dir, String args1) {
		
		return args1.endsWith(".text");
	}

}
