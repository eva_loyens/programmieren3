package at.campus02.nowa.prg3.klausur.networkio;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class Client {

	public static void main(String[] args) {
		try (
				Socket client = new Socket(InetAddress.getLocalHost(), 4000);
				BufferedReader br = new BufferedReader(new InputStreamReader(client.getInputStream()));
				PrintWriter pw = new PrintWriter(client.getOutputStream());
			) {
				String line;
				while (true) {
					pw.println("ping");
					pw.flush();
					line = br.readLine();
					Thread.sleep(500);
					if (line == null || !line.equals("pong")) {
						break;
					}
					System.out.print("#");
				}
			} catch (UnknownHostException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
	}

}
