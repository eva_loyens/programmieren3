package at.campus02.nowa.prg3.klausur.networkio;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Random;

public class ClientRunnable implements Runnable {

	private Socket socket;
	private int rounds; // angabe
	
	public ClientRunnable(Socket socket) {
		this.socket = socket;
		this.rounds = new Random().nextInt(10);
	}
	
	@Override
	public void run() {
				
	try(
		BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
				// printwriter: dann muss mann sich nicht um zeilenumbrueche kuemmern //
			
		PrintWriter pw = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));
			) {
		 		for (int i = 0; i < rounds; i++) {
				String line = br.readLine();
				if (line == null) {
					return;
				}
				
				pw.println("pong");
				pw.flush();
				// FLUSH NICHT VERGESSEN!!
				}
			String line = br.readLine();
			if (line == null) {
				return;
			}
			// 3 zeilen sind gleichwertig, aber mann muss beim print/write \n HINZUFUEGEN; SONNST HAENGT DER CLIENT
			// schreibe also einfach println
			// pw.print("stop\n");
			// pw.write("stop\n");
			pw.println("stop");
			pw.flush();
			} catch (IOException e) {
						e.printStackTrace();
			}
	}
}