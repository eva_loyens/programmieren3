package at.campus02.nowa.ss2018.fileio;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;


public class InputStreamDemo_juni06 {

	public static void main(String[] args) {
		try {
			File f = new File("test1.txt");
			long length = f.length();
			System.out.println(length);
			InputStream inp = new FileInputStream("test1.txt");

			int b;
			while ((b = inp.read()) != -1) {
				System.out.println(b);
				}
			inp.close();
			
			} catch (FileNotFoundException e) {
			e.printStackTrace();
			} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
