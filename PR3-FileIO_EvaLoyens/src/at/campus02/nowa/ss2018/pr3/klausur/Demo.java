package at.campus02.nowa.ss2018.pr3.klausur;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStreamWriter;

public class Demo {

	public static void main(String[] args) {
		
		char a = ' '; 

		try (
				ByteCounterInputStream is = new ByteCounterInputStream(new FileInputStream("test.dat"), a);
				OutputStreamWriter os = new OutputStreamWriter(System.out)
			 )
		{
			int b;
			while ((b = is.read()) != -1) {
			char c = Character.toChars(b)[0];
			if (c == ' ') {
				return;
					}
			System.out.print("Anzahl der Leerzeichen: " + ((ByteCounterInputStream) is).getCount());
				os.write(b);
				os.flush();
			}
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

}
