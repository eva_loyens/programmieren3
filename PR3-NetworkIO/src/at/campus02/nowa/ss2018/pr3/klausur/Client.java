package at.campus02.nowa.ss2018.pr3.klausur;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class Client {

	public static void main(String[] args) {
		try (
			BufferedReader brc = new BufferedReader(new InputStreamReader(System.in));
			Socket socket = new Socket(InetAddress.getLocalHost(), 1111);
			BufferedReader brs = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			PrintWriter pws = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));
		) {
			while (true) {
				System.out.println("Bitte Befehl eingeben:");
				String line = brc.readLine();
				if (line == null) {
					System.out.println("Console input closed, terminating program");
					break;
				}
				pws.println(line);
				pws.flush();
				String answer = brs.readLine();
				if (answer == null) {
					System.out.println("Server stream closed, terminating program");
					break;
				}
				System.out.println("Server: " + answer);

			}

		} catch (UnknownHostException e) {
			System.out.println("Could not resolve remote host: " + e.getMessage());
		} catch (IOException e) {
			System.out.println("Could not connect to remote host: " + e.getMessage());
		}

	}

}
