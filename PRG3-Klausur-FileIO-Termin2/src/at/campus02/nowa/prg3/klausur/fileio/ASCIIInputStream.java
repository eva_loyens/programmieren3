package at.campus02.nowa.prg3.klausur.fileio;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

public class ASCIIInputStream extends FilterInputStream {

	protected ASCIIInputStream(InputStream in) {
		super(in);
	}

	@Override
	public int read() throws IOException {
		
		int b;
		while ((b = super.read()) !=-1) {
			if(b < 128) {
				return b;
			}
		}
		return b;
	}
}
