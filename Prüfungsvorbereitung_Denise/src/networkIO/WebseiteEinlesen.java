package networkIO;
import java.io.*;
import java.net.*;

public class WebseiteEinlesen
{

	public static void main(String[] args) throws IOException
	{
		URL url = new URL("https://wetter.orf.at");

		InputStream stream = url.openStream();

		BufferedReader reader = new BufferedReader(new InputStreamReader(stream));

		String line;

		while ((line = reader.readLine()) != null)
		{
			System.out.println(line);
		}
	}

}
