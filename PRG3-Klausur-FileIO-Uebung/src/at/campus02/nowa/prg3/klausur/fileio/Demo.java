package at.campus02.nowa.prg3.klausur.fileio;

import java.io.FileInputStream;
import java.io.IOException;
// das hier ist klausur futter!!

public class Demo {
	// schoen wen es ein try ressources block gibt!!
	// bei run und dann ausgabe auf der console darf keinen wert groesser als 128 vorkommen!!

	public static void main(String[] args) {
		
		try (ASCIIInputStream ais  = new ASCIIInputStream(new FileInputStream("input.txt")); )
		{
			int b;
			while((b = ais.read()) != -1) {
				System.out.println(b);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

}
